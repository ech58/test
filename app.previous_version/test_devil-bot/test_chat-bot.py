from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return "Index Page"

@app.route('/hello')
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/chat-bot')
def chat_bot():
    return render_template('chatbot_template.html')
